# Fetch Functions

This repository contains code for executing functions locally or through rpc if function has not been implemented locally. If user chooses to run function locally but function has not been implemented locally, .so is fetched from server and saved on disk of future use.

## How to Run
1. Download and build rpclib
   - git clone git@github.com:rpclib/rpclib.git
   - cd rpclib
   - mkdir build
   - cd build
   - cmake ..
   - cmake --build .
   - make install

2. Include headers in $PATH
   - PATH="/home/username/rpclib/include/rpc:$PATH"

3. Compile all function files separately using
   - "g++ --std=c++17 -fPIC -rdynamic -shared -o ./add.so ./add.cpp"

4. Compile main program in Client and Server using
   - g++ --std=c++17 -rdynamic -o main.out main.cpp dynamic_lib.cpp -ldl -lrpc -lpthread

5. Run main from server first and then client using
   - "./main.out"


### Author
Tehreem Fatima (18030009@lums.edu.pk)
