#include <iostream>
#include <fstream>
#include "rpc/server.h"
#include "rpc/this_handler.h"
#include <string.h>
#include "dynamic_lib.h"

using namespace std;

const int PORT = 20143;

void * get_func(string func_name) {
	dynamic_lib * lib = new dynamic_lib();
	lib->set_path("./Functions/"+func_name+".so");
	lib->load_lib();
	void * fetched_func = lib->fetch_function(func_name);
	return fetched_func;
}

double run_on_server(string func_name, double a, double b) {
	void * f_func = get_func(func_name);
	double result = ((double (*)(double, double))f_func)(a, b);
	return result;
}

string send_file(string func_name) {
	cout<<"request receveived to send file to client: "<<func_name<<endl;
	string file_path = "./Functions/"+func_name+".so";
	ifstream file(file_path);
  string content((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
  return content;
}

int main(int argc, char const *argv[]) {
	rpc::server srv(PORT);
  srv.bind("run_on_server", &run_on_server);
  srv.bind("send_file", &send_file);

	srv.async_run(2);
	std::cout << "Press [ENTER] to exit the server." << std::endl;
	std::cin.ignore();

  return 0;
}
