#include "dynamic_lib.h"
using namespace std;
// Constructor
dynamic_lib::dynamic_lib() {
  path = "";
  handle = nullptr;
}
// End - Constructor

// Getter Functions
dynamic_lib_handle dynamic_lib::get_handle() {
  return handle;
}

string dynamic_lib::get_path() {
  return path;
}
// End - Getters

// Setter Functions
void dynamic_lib::set_handle(dynamic_lib_handle _handle) {
  handle = _handle;
}

void dynamic_lib::set_path(string _path) {
  path = _path;
}
// End - Setters

// Member Functions
void dynamic_lib::load_lib() {
  string _path = get_path();
  if(_path != "") {
  	cout << "Trying to open: " << _path << endl;
    set_handle(dlopen(_path.data() , RTLD_NOW)); // get a handle to the lib, may be nullptr.
		// RTLD_NOW ensures that all the symbols are resolved immediately. This means that
		// if a symbol cannot be found, the program will crash now instead of later.
  } else {
    cout<<"Error: Path not set"<<endl;
  }
}

void * dynamic_lib::fetch_function(string func_name) {
  dynamic_lib_handle _handle = get_handle();
  return dlsym(_handle , func_name.c_str());
}

void dynamic_lib::close_lib() {
  dynamic_lib_handle _handle = get_handle();
	dlclose(_handle);
}
// End - Member Functions

// Destructor
dynamic_lib::~dynamic_lib() {
  dynamic_lib_handle _handle = get_handle();
	if (_handle != nullptr)
		close_lib();
}
// End - Destructor
