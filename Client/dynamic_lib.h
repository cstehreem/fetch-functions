#include <dlfcn.h>
#include <iostream>
typedef void* dynamic_lib_handle;

class dynamic_lib {
private:
	dynamic_lib_handle  handle;
	std::string path;
public:
  dynamic_lib();
  dynamic_lib_handle get_handle();
  std::string get_path();
  void set_handle(dynamic_lib_handle _handle);
  void set_path(std::string _path);
  void load_lib();
  void * fetch_function(std::string func_name);
  void close_lib();
	~dynamic_lib();
};
