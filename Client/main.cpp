#include <iostream>
#include <fstream>
#include "rpc/client.h"
#include "rpc/rpc_error.h"
#include <string.h>
#include "dynamic_lib.h"

using namespace std;

const int PORT = 20143;

string getFuncName(char op);

int main(void) {
	// Init RPC client
    rpc::client c("localhost", PORT);

	// Loading DLLs
	char op;
	double input_1;
	double input_2;
	string func_name = "";
	while (true) {
		cout<<"Choose an operation: + - * /"<<endl;
		cin>>op;
		func_name = getFuncName(op);
		if(func_name != "") {
			dynamic_lib * lib = new dynamic_lib();
			lib->set_path("./Functions/"+func_name+".so");
			lib->load_lib();
			cout<<"First input: "<<endl;
			cin>>input_1;
			cout<<"Second input: "<<endl;
			cin>>input_2;
      char selection = '0';
      while(1) {
        cout<<"Do you want to run the function locally? (y/n) ";
        cin>>selection;
        if(selection == 'y' || selection == 'n') {
          break;
        }
      }
      switch (selection) {
        case 'y': {
          // User chose to run function locally
          try
          {
            void * fetched_func = lib->fetch_function(func_name);
            double result = ((double (*)(double, double))fetched_func)(input_1, input_2);
            cout<< "Function available locally"<<endl;
            cout<< "Result: ";
            cout << result << endl;
          }
          catch (int err_code)
          {
            if(err_code == -1) {
              cout<< "Function not available locally, requesting server"<<endl;
              try {
                lib->close_lib();
                //fetch file here and replace with already present file, then execut
                auto file = c.async_call("send_file", func_name);
                string file_contents = file.get().as<string>();
                ofstream outfile("Functions/"+func_name+".so");
                long int size = file_contents.length();
                outfile.write(file_contents.c_str(), size);
                outfile.close();

                cout<< "Function fetched from server, executing locally"<<endl;
                // Now calling the function from new dll
          			lib->set_path("./Functions/"+func_name+".so");
          			lib->load_lib();
                void * fetched_func = lib->fetch_function(func_name);
                double result = ((double (*)(double, double))fetched_func)(input_1, input_2);
                cout<< "Result: "<<result<<endl;
              } catch (rpc::rpc_error &e) {
                  std::cout << std::endl << e.what() << std::endl;
                  std::cout << "in function '" << e.get_function_name() << "': ";

                  using err_t = std::tuple<int, std::string>;
                  auto err = e.get_error().as<err_t>();
                  std::cout << "[error " << std::get<0>(err) << "]: " << std::get<1>(err)
                            << std::endl;
                  return 1;
              }
            }
          }
          break;
        }
        case 'n': {
          // User chose to run function on server
          try {
            // Request server to execute the function and send back result
            double result = c.call("run_on_server", func_name, input_1, input_2).as<double>();
            cout<<"Response received from server: "<<result<<endl;
          } catch (rpc::rpc_error &e) {
              std::cout << std::endl << e.what() << std::endl;
              std::cout << "in function '" << e.get_function_name() << "': ";

              using err_t = std::tuple<int, std::string>;
              auto err = e.get_error().as<err_t>();
              std::cout << "[error " << std::get<0>(err) << "]: " << std::get<1>(err)
                        << std::endl;
              return 1;
          }
          break;
        }
      }
			delete lib;
		} else {
			continue;
		}
	}
}

string getFuncName(char op) {
	switch(op) {
		case '+' : {
			return "add";
		}
		case '-' : {
			return "subtract";
		}
		case '*' : {
			return "multiply";
		}
		case '/' : {
			return "divide";
		}
		default : {
			cout<<"You can only choose one of mentioned operations"<<endl;
			return "";
		}
	}
}
